#!/usr/bin/env python
#
"""
%prog [options] amqp_server

Monitor an AMQP message queue for replies from an MLF2 float and email the
contents to the original sender. The sender's email address must be stored
in the reply_to message header.
"""
import pika
import smtplib
import logging
import logging.handlers
import os
from email.mime.text import MIMEText
from optparse import OptionParser
from functools import partial


class MqInterface(object):
    """
    Class to manage the asynchronous setup process for the AMQP
    message-queue interface.
    """
    def __init__(self, host, credentials, handlers=None):
        self.channel = None
        self.conn = None
        self.consumer_tag = None
        self.handlers = handlers or []
        self.logger = logging.getLogger()
        self.exchanges = set([e[0] for e in handlers])
        creds = pika.PlainCredentials(*credentials)
        self.params = pika.ConnectionParameters(host=host,
                                                virtual_host='mlf2',
                                                credentials=creds,
                                                heartbeat_interval=300)

    def start(self):
        self.conn = pika.SelectConnection(self.params,
                                          on_open_callback=self.on_connection_open)
        self.conn.set_backpressure_multiplier(10000)
        self.conn.ioloop.start()

    def stop(self):
        self.conn.close()
        self.conn.ioloop.start()
        self.channel = None

    @property
    def connection(self):
        return self.conn

    def on_connection_open(self, conn):
        self.logger.info('Connected to server')
        self.conn = conn
        conn.channel(self.on_channel_open)

    def on_channel_open(self, ch):
        self.channel = ch
        for name in self.exchanges:
            self.channel.exchange_declare(
                callback=partial(self.on_exchange_declared, name),
                exchange=name,
                type='topic',
                durable=True)

    def on_exchange_declared(self, name, frame):
        self.logger.info('Exchange declared (%s)', name)
        for exch, key, func in self.handlers:
            if exch != name:
                continue
            kwds = {'routing_key': key, 'func': func, 'exchange': exch}
            if ('#' in key) or ('*' in key):
                self.channel.queue_declare(
                    callback=partial(self.on_queue_declared, **kwds),
                    exclusive=True)
            else:
                # Using an existing queue. No need to bind.
                kwds = {'qname': key, 'func': func}
                self.channel.queue_declare(queue=key,
                                           durable=True,
                                           callback=partial(
                                               self.on_queue_bound, **kwds),
                                           exclusive=False)

    def on_queue_declared(self, frame, routing_key='', func=None, exchange=''):
        qname = frame.method.queue
        self.logger.info('Queue %s declared', qname)
        kwds = {'func': func, 'qname': qname}
        self.channel.queue_bind(callback=partial(self.on_queue_bound, **kwds),
                                exchange=exchange,
                                queue=qname,
                                routing_key=routing_key)

    def on_queue_bound(self, frame, qname='', func=None):
        self.logger.info('Queue %s bound', qname)
        self.channel.basic_consume(func, queue=qname, no_ack=True)


def handle_reply(ch, method, properties, body):
    """
    AMQP consumer function that forwards the reply message via email.
    """
    logger = logging.getLogger()
    sender = 'mlf2-noreply@apl.uw.edu'
    headers = properties.headers
    msg = MIMEText(str(body))
    msg['To'] = properties.reply_to
    msg['From'] = sender
    msg['Subject'] = 'reply from float-%d' % headers['floatid']
    s = smtplib.SMTP(os.environ.get('MAILHOST', 'localhost'))
    logger.info('Forwarding reply to %s', properties.reply_to)
    s.sendmail(sender, [properties.reply_to], msg.as_string())
    s.quit()


def mainloop(server, opts):
    if opts.credentials:
        credentials = open(opts.credentials, 'r').readline().strip().split(':')
    else:
        credentials = 'guest', 'guest'

    events = [('commands', '*.replies', handle_reply)]
    mq = MqInterface(server, credentials, handlers=events)
    try:
        mq.start()
    finally:
        mq.stop()


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(credentials=None)
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='FILENAME',
                      help='credentials file for server access')

    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing AMQP server name')

    mainloop(args[0], opts)

if __name__ == '__main__':
    main()
