#!/usr/bin/env python
#
# Interface between the new MLF2 Message Queue system and the legacy Django-based
# database system.
#
"""
%prog [options] directory

Pull MLF2 status messages, command responses, and datafiles; write them to the named directory and
run the legacy data-processing program.
"""
import pika
import os
import subprocess
import logging
import logging.handlers
import zlib
try:
    import simplejson as json
except ImportError:
    import json
import cPickle as pickle
from daemon import DaemonContext
from optparse import OptionParser
from functools import partial
from collections import defaultdict
from pika.reconnection_strategies import SimpleReconnectionStrategy

class ReplyDB(object):
    """
    Maintain a database of partial float responses indexed
    by message-id
    """
    def __init__(self, filename):
        self.filename = filename
        self.load()

    def load(self):
        try:
            f = open(self.filename, 'rb')
            self.db = pickle.load(f)
            f.close()
        except IOError:
            self.db = defaultdict(list)

    def store(self):
        f = open(self.filename, 'wb')
        pickle.dump(self.db, f)
        f.close()

    def __setitem__(self, key, value):
        self.db[key].append(value)

    def __getitem__(self, key):
        return self.db[key]

    def __delitem__(self, key):
        del self.db[key]


class MqInterface(object):
    """
    Class to manage the asynchronous setup process for the AMQP message-queue interface.
    """
    def __init__(self, host, credentials, handlers=[]):
        self.channel = None
        self.conn = None
        self.consumer_tag = None
        self.handlers = handlers
        self.logger = logging.getLogger()
        self.exchanges = set([e[0] for e in handlers])
        creds = pika.PlainCredentials(*credentials)
        self.params = pika.ConnectionParameters(host=host,
                                                virtual_host='mlf2',
                                                credentials=creds,
                                                heartbeat_interval=300)

    def start(self):
        rs = SimpleReconnectionStrategy()
        self.conn = pika.SelectConnection(self.params,
                                          on_open_callback=self.on_connection_open,
                                          reconnection_strategy=rs)
        self.conn.set_backpressure_multiplier(10000)
        self.conn.ioloop.start()

    def stop(self):
        self.conn.close()
        self.conn.ioloop.start()
        self.channel = None

    @property
    def connection(self):
        return self.conn

    def on_connection_open(self, conn):
        self.logger.info('Connected to server')
        self.conn = conn
        conn.channel(self.on_channel_open)

    def on_channel_open(self, ch):
        self.channel = ch
        for name in self.exchanges:
            self.channel.exchange_declare(callback=partial(self.on_exchange_declared,
                                                           name),
                                          exchange=name,
                                          type='topic',
                                          durable=True)

    def on_exchange_declared(self, name, frame):
        self.logger.info('Exchange declared (%s)', name)
        for exch, key, func in self.handlers:
            if exch != name:
                continue
            kwds = {'routing_key': key, 'func': func, 'exchange': exch}
            if ('#' in key) or ('*' in key):
                self.channel.queue_declare(callback=partial(self.on_queue_declared, **kwds),
                                           exclusive=True)
            else:
                # Using an existing queue. No need to bind.
                kwds = {'qname': key, 'func': func}
                self.channel.queue_declare(queue=key,
                                           durable=True,
                                           callback=partial(self.on_queue_bound, **kwds),
                                           exclusive=False)

    def on_queue_declared(self, frame, routing_key='', func=None, exchange=''):
        qname = frame.method.queue
        self.logger.info('Queue %s declared', qname)
        kwds = {'func': func, 'qname': qname}
        self.channel.queue_bind(callback=partial(self.on_queue_bound, **kwds),
                                exchange=exchange,
                                queue=qname,
                                routing_key=routing_key)

    def on_queue_bound(self, frame, qname='', func=None):
        self.logger.info('Queue %s bound', qname)
        self.channel.basic_consume(func, queue=qname, no_ack=True)


def write_reply_file(outf, id, rows):
    """
    Write the full reply from the float to a text file.
    """
    outf.write(id + '\n')
    for row in rows:
        outf.write('>'+row['q']+'\n')
        outf.write(row['a']+'\n')

def response_handler(db, datadir, prog, ch, method, properties, body):
    logger = logging.getLogger()
    imei = properties.headers.get('imei')
    msg_id = properties.headers.get('msg_id')
    if imei and msg_id:
        logger.info('Processing command response (%s)', msg_id)
        db[msg_id] = json.loads(body)
        if properties.headers.get('eot'):
            logger.info('Command reply complete')
            os.environ['IRSN'] = imei
            os.environ['LEGACYDB'] = '1'
            floatdir = os.path.join(datadir, imei)
            outf = open(os.path.join(floatdir, 'reply.txt'), 'w')
            write_reply_file(outf, msg_id, db[msg_id])
            outf.close()
            subprocess.call([prog, imei], cwd=floatdir, env=os.environ)
            del db[msg_id]
        db.store()

def status_handler(datadir, prog, ch, method, properties, body):
    logger = logging.getLogger()
    imei = properties.headers.get('imei')
    if imei:
        os.environ['IRSN'] = imei
        os.environ['LEGACYDB'] = '1'
        floatdir = os.path.join(datadir, imei)
        outf = open(os.path.join(floatdir, 'status.xml'), 'w')
        outf.write(body)
        outf.close()
        logger.info('Processing status message')
        subprocess.call([prog, imei], cwd=floatdir, env=os.environ)

def decompress_to_file(contents, outf):
    """
    Decompress a gzip-compressed string to an output file.
    """
    # We cannot use the gzip module because the string might contain
    # "extra" bytes at the end (after the compressed content). While
    # this is allowed in the gzip format, the Python gzip module
    # cannot deal with it and will throw an exception.
    #
    # Basic gzip header length
    hdrlen = 10
    # Null-terminated filename is appended to the header if the FNAME
    # flag is set.
    if (ord(contents[3]) & 0x08) == 0x08:
        namelen = contents[hdrlen:].find('\x00')
        hdrlen = hdrlen + namelen + 1
    outf.write(zlib.decompress(contents[hdrlen:], -zlib.MAX_WBITS))

def file_handler(datadir, prog, ch, method, properties, body):
    logger = logging.getLogger()
    imei = properties.headers.get('imei')
    filename = properties.headers.get('filename')
    if imei and filename:
        os.environ['IRSN'] = imei
        os.environ['LEGACYDB'] = '1'
        floatdir = os.path.join(datadir, imei)
        outf = open(os.path.join(floatdir, filename), 'w')
        if properties.content_encoding == 'gzip':
            decompress_to_file(body, outf)
        else:
            outf.write(body)
        outf.close()
        logger.info('Processing data file %s', filename)
        subprocess.call([prog, imei], cwd=floatdir, env=os.environ)

def setup_logger(logfile=None):
    lgr = logging.getLogger()
    lgr.setLevel(logging.INFO)
    fmt = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y/%m/%d %H:%M:%S')
    if logfile:
        lh = logging.handlers.RotatingFileHandler(logfile, maxBytes=1024*1024, backupCount=10)
    else:
        lh = logging.StreamHandler()
    lh.setFormatter(fmt)
    lgr.addHandler(lh)
    return lgr

def mainloop(dirname, opts):
    setup_logger(logfile=opts.background and opts.logfile)

    if opts.credentials:
        credentials = open(opts.credentials, 'r').readline().strip().split(':')
    else:
        credentials = 'guest', 'guest'

    db = ReplyDB(opts.replydb)
    events = [('data', '*.status', partial(status_handler, dirname, opts.proc)),
              ('data', '*.file', partial(file_handler, dirname, opts.proc)),
              ('commands', '*.responses',
               partial(response_handler, db, dirname, opts.proc))]

    mq = MqInterface(opts.server, credentials, handlers=events)
    try:
        mq.start()
    finally:
        mq.stop()

def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(server='localhost', credentials=None, proc='dataproc',
                        logfile=None, background=True,
                        replydb=os.path.expanduser('~/.local/share/reply.db'))
    parser.add_option('-s', '--server',
                      type='string',
                      dest='server',
                      metavar='HOST',
                      help='specify AMQP server host (default: %default)')
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='FILENAME',
                      help='credentials file for server access')
    parser.add_option('-p', '--proc',
                      type='string',
                      dest='proc',
                      metavar='PROGNAME',
                      help='processing program to run (default: %default)')
    parser.add_option('-n', '--no-daemon',
                      action='store_false',
                      dest='background',
                      help='keep process in the foreground')
    parser.add_option('-l', '--logfile',
                      type='string',
                      dest='logfile',
                      help='name of log-file')

    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing directory name')

    if opts.background:
        context = DaemonContext(working_directory=os.environ['HOME'])
        with context:
            mainloop(args[0], opts)
    else:
        mainloop(args[0], opts)

if __name__ == '__main__':
    main()
