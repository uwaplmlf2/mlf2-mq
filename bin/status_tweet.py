#!/usr/bin/env python
#
"""%prog [options] cfgfile

Push MLF2 status updates from the Message Queue to Twitter. The
configuration file is in JSON format and must contain an object named
'twitter' with the following attributes:

  consumer_key: "twitter app Consumer Key"
  consumer_secret: "twitter app Consumer Secret"
  auth_token: "twitter app Access Token"
  auth_secret: "twitter app Access Token Secret"

"""

import pika
import logging
import tweepy
import math
import os
from httplib2 import Http
from daemon import DaemonContext
try:
    import simplejson as json
except ImportError:
    import json
try:
    from lxml import etree
except ImportError:
    from xml.etree import ElementTree as etree
from collections import namedtuple
from decimal import Decimal
from datetime import datetime
from dateutil.tz import tzutc
from functools import partial
from optparse import OptionParser


URLTMPL = 'http://mlf2data.apl.uw.edu:5984/mlf2db/_design/status/_show/gmap/%(db_id)s'
OAuth = namedtuple('OAuth', 'consumer_key consumer_secret auth_token auth_secret')


class Dmm(object):
    """
    Class to represent angles as degrees and decimal minutes
    """
    deg_exp = Decimal('1')
    min_exp = Decimal('0.0001')

    def __init__(self, deg):
        self.deg = deg
        if self.deg < 0.:
            self.deg = math.ceil(self.deg)
        elif self.deg > 0.:
            self.deg = math.floor(self.deg)
        self.min = (deg - self.deg)*60.

    def __str__(self):
        d = Decimal(str(self.deg))
        m = Decimal(str(abs(self.min)))
        padding = (m < 10.) and '0' or ''
        return '%s-%s%s' % (d.quantize(self.deg_exp), padding, m.quantize(self.min_exp))

    def to_degrees(self):
        return self.deg + self.min/60.

    def is_neg(self):
        return self.deg < 0.

    def __abs__(self):
        d = Dmm(0.)
        d.deg = abs(self.deg)
        d.min = abs(self.min)
        return d


def twitter_access(oauth):
    """
    Initialize access to the Twitter API.
    """
    auth = tweepy.OAuthHandler(oauth.consumer_key, oauth.consumer_secret)
    auth.set_access_token(oauth.auth_token, oauth.auth_secret)
    return tweepy.API(auth)


def parse_status_date(s):
    return datetime.strptime(s, '%Y-%m-%d %H:%M:%S').replace(tzinfo=tzutc()).isoformat()


def parse_gps(elem):
    """
    Parse gps XML element and return a dictionary.
    """
    d = {}
    d['lat'], d['lon'] = [float(e) for e in elem.text.split('/')]
    d['nsat'] = int(elem.get('nsats'))
    return d


def convert_xml_status(root):
    """
    Convert an MLF2 XML status message to a dictionary
    """
    d = {}
    if root.tag == 'status':
        d['mode'] = 'mission'
    else:
        d['mode'] = root.tag
    d['timestamp'] = parse_status_date(root.findtext('date'))
    d['gps'] = parse_gps(root.find('gps'))
    sens = {'ipr': float(root.findtext('ipr')),
            'rh': float(root.findtext('rh')),
            'voltage': [int(v) for v in root.findtext('v').split('/')]}
    d['sensors'] = sens
    d['irsq'] = int(root.findtext('irsq'))
    d['piston_error'] = int(root.findtext('ep'))
    d['freespace'] = int(root.findtext('df'))
    alerts = set()
    for ele in root.findall('alert'):
        alerts.add(ele.text)
    d['alerts'] = list(alerts)
    return d


def create_tweet(status):
    lat = Dmm(status['gps']['lat'])
    lon = Dmm(status['gps']['lon'])
    hlat = lat.is_neg() and 'S' or 'N'
    hlon = lon.is_neg() and 'W' or 'E'

    msg = '#mlf2_%d %s%s %s%s %d %s' % (status['floatid'],
                                        str(abs(lat)), hlat,
                                        str(abs(lon)), hlon,
                                        status['gps']['nsat'],
                                        status['timestamp'])
    logging.info('Tweet: %s', msg)
    return '\n'.join([msg] + status['alerts'])


def shorten_url(url, server='www.googleapis.com',
                path='/urlshortener/v1/url',
                apikey=None):
    """
    Use Google's URL Shortening Service for any URL included in the
    Tweet. Twitter's native URL shortener cannot handle IP addresses
    (!!!).
    """
    h = Http(timeout=10)
    resource = dict(longUrl=url)
    if apikey:
        svc_url = 'https://%s/%s?key=%s' % (server, path, apikey)
    else:
        svc_url = 'https://%s/%s' % (server, path)
    resp, content = h.request(svc_url, 'POST',
                              body=json.dumps(resource),
                              headers={'Content-Type': 'application/json'})
    result = json.loads(content)
    return result['id']


def status_update(api, exclude, ch, method, props, body):
    """
    Message queue consumer. Parse the status message (XML or JSON) and create a
    summary Tweet.
    """
    floatid = props.headers.get('floatid')
    group = props.headers.get('group')
    if group in exclude:
        logging.info('Skipping message from float %d (group %s)', floatid,
                     group)
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return
    logging.info('Status message from float %d', floatid)
    try:
        if props.content_type == 'application/xml':
            doc = convert_xml_status(etree.fromstring(body))
        else:
            doc = json.loads(body)
    except Exception as e:
        logging.error('Cannot parse status message %s (%s)', repr(body), str(e))
        return
    doc['floatid'] = floatid
    msg = create_tweet(doc)
    if 'db_id' in props.headers and len(msg) < 116:
        # Append a URL that can be used to view the status information
        # in a browser
        try:
            u = URLTMPL % props.headers
            u = shorten_url(u, apikey=os.environ.get('GOOGLE_API_KEY'))
        except Exception as e:
            logging.error('Cannot shorten status URL (%s)', str(e))
            u = ''
        msg = msg + ' ' + u
    try:
        if len(msg) > 140:
            msg = msg[0:137] + '...'
        api.update_status(msg, lat=doc['gps']['lat'], long=doc['gps']['lon'])
        ch.basic_ack(delivery_tag=method.delivery_tag)
    except tweepy.TweepError as e:
        logging.error('Twitter error: %s', str(e))
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=True)
    except Exception as e:
        logging.error('Caught exception: %s', str(e))
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)


def mq_setup(host, credentials):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds,
                                       heartbeat_interval=300)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()
    return channel


def mainloop(cfg, opts):
    logging.basicConfig(level=logging.INFO)
    auth = OAuth(**cfg['twitter'])
    api = twitter_access(auth)
    credentials = str(cfg['mq']['user']), str(cfg['mq']['password'])

    channel = mq_setup(str(cfg['mq']['host']), credentials)
    channel.exchange_declare(exchange=opts.exchange,
                             durable=True,
                             type='topic')
    result = channel.queue_declare(durable=False,
                                   exclusive=True,
                                   auto_delete=True)
    channel.queue_bind(exchange=opts.exchange,
                       queue=result.method.queue,
                       routing_key=opts.binding_key)
    channel.basic_consume(partial(status_update, api, opts.exclude),
                          queue=result.method.queue,
                          no_ack=False)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        return 0
    except Exception as e:
        logging.error('Aborting on exception: %s', str(e))
        return 1


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(background=True,
                        exchange='data', logfile=None,
                        binding_key='*.status.processed',
                        exclude=[])
    parser.add_option('-n', '--no-daemon',
                      action='store_false',
                      dest='background',
                      help='keep process in the foreground')
    parser.add_option('-e', '--exchange',
                      type='string',
                      dest='exchange',
                      metavar='NAME',
                      help='AMQP exchange to use (default: %default)')
    parser.add_option('-l', '--logfile',
                      type='string',
                      dest='logfile',
                      metavar='FILENAME',
                      help='name of log-file')
    parser.add_option('-b', '--binding-key',
                      type='string',
                      dest='binding_key',
                      metavar='KEY',
                      help='queue binding for status messages (default: %default)')
    parser.add_option('-g', '--exclude-group',
                      dest='exclude',
                      metavar='GROUP',
                      action='append',
                      help='exclude the named float group')

    opts, args = parser.parse_args()
    cfg = dict()
    try:
        cfg = json.load(open(args[0], 'r'))
    except IndexError:
        parser.error('Missing configuration file')
    except Exception as e:
        parser.error('Invalid configuration file: %s (%s)' % (args[0], repr(e)))

    if opts.background:
        logf = None
        if opts.logfile:
            logf = open(opts.logfile, 'w+')
        context = DaemonContext(stderr=logf,
                                working_directory=os.environ['HOME'])
        with context:
            mainloop(cfg, opts)
    else:
        mainloop(cfg, opts)


if __name__ == '__main__':
    main()
