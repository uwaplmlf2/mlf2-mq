#!/usr/bin/env python
#
"""
%prog [options] floatid filename [filename ...]

Add one or more files to the MLF2 message queue. The file contents are
published to the 'data' exchange using the routing key 'float-N.files'.
"""
import pika
import os
import time
import re
from optparse import OptionParser


BP = pika.BasicProperties


def timestamp():
    """
    Return a microsecond-resolution timestamp.
    """
    return long(time.time() * 1000000L)


def mq_setup(host, credentials):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()

    channel.exchange_declare(exchange='data',
                             type='topic',
                             durable=True)
    return conn, channel


def guess_type(filename):
    """
    Try to guess the mime-type and encoding of the named file.

    >>> guess_type('syslog01.txt')
    ('text/x-syslog', None)
    >>> guess_type('syslog01.z')
    ('text/x-syslog', 'gzip')
    >>> guess_type('syslog0.z')
    ('text/x-syslog', 'gzip')
    >>> guess_type('syslog0.txt')
    ('text/x-syslog', None)
    >>> guess_type('00index.z')
    ('text/x-listing', 'gzip')
    """
    table = ((r'.*\.ncz', ('application/x-netcdf', 'gzip')),
             (r'.*\.nc$', ('application/x-netcdf', None)),
             (r'syslog\d*\.txt|errors\.txt', ('text/x-syslog', None)),
             (r'.*\.txt', ('text/plain', None)),
             (r'.*\.sx$', ('text/x-sexp', None)),
             (r'.*\.sxz', ('text/x-sexp', 'gzip')),
             (r'.*\.csv', ('text/csv', None)),
             (r'.*\.xml', ('application/xml', None)),
             (r'trk\d*\.z|eng\d*\.z', ('text/csv', 'gzip')),
             (r'syslog\d*\.z|errors\.z', ('text/x-syslog', 'gzip')),
             (r'.*\.jpg', ('image/jpeg', None)),
             (r'00index\.z', ('text/x-listing', 'gzip')),
             (r'00index', ('text/x-listing', None)),
             (r'.*\.z', ('text/plain', 'gzip')))

    for pattern, result in table:
        if re.match(pattern, filename, re.IGNORECASE):
            return result
    return 'application/octet-stream', None


def publish_file(channel, float_id, pathname, **kwds):
    def quick_look_filename(infile):
        _, ext = os.path.splitext(infile)
        return 'ql-' + time.strftime('%Y%m%d_%H%M%S') + ext
    headers = {'floatid': float_id}
    filename = os.path.basename(pathname)
    if filename.startswith('status.'):
        publish_status(channel, float_id, pathname, **kwds)
        return
    # Generate a unique name for quick-look files
    for prefix in ('telem', 'ql.', 'dataql.'):
        if filename.startswith(prefix):
            filename = quick_look_filename(filename)
            break
    mimetype, enc = guess_type(filename)
    if enc == 'gzip':
        # Determine the name of the uncompressed file
        base, ext = os.path.splitext(filename)
        if mimetype == 'application/x-netcdf':
            filename = base + '.nc'
        elif mimetype == 'text/x-sexp':
            filename = base + '.sx'
        elif mimetype == 'text/csv':
            filename = base + '.csv'
        else:
            filename = base + '.txt'
    headers['filename'] = filename
    headers.update(kwds)
    channel.basic_publish(exchange='data',
                          routing_key='float-%d.file' % float_id,
                          body=open(pathname, 'rb').read(),
                          properties=BP(delivery_mode=2,
                                        headers=headers,
                                        timestamp=timestamp(),
                                        content_type=mimetype,
                                        content_encoding=enc))


def publish_status(channel, float_id, pathname, **kwds):
    headers = {'floatid': float_id}
    _, ext = os.path.splitext(pathname)
    if ext == '.xml':
        mimetype = 'application/xml'
    else:
        mimetype = 'application/json'
    headers.update(kwds)
    channel.basic_publish(exchange='data',
                          routing_key='float-%d.status' % float_id,
                          body=open(pathname, 'rb').read(),
                          properties=BP(delivery_mode=2,
                                        headers=headers,
                                        timestamp=timestamp(),
                                        content_type=mimetype))


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(server='localhost', credentials=None)
    parser.add_option('-s', '--server',
                      type='string',
                      dest='server',
                      metavar='HOST',
                      help='specify AMQP server host (default: %default)')
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='FILENAME',
                      help='credentials file for server access')

    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    float_id = int(args[0])
    if opts.credentials:
        credentials = open(opts.credentials, 'r').readline().strip().split(':')
    else:
        credentials = 'guest', 'guest'

    conn, channel = mq_setup(opts.server, credentials)
    for pathname in args[1:]:
        publish_file(channel, float_id, pathname)

if __name__ == '__main__':
    main()
